/* 1. Buat Database */
CREATE DATABASE myshop;

/* 2. Buat Tabel */
USE myshop;

CREATE TABLE users (
	id INT AUTO_INCREMENT,
	name VARCHAR(255),
	email VARCHAR(255),
	password VARCHAR(255),
	PRIMARY KEY(id));

CREATE TABLE categories (
	id INT AUTO_INCREMENT,
	name VARCHAR(255),
	PRIMARY KEY(id));

CREATE TABLE items (
	id INT AUTO_INCREMENT,
	name VARCHAR(255),
	description VARCHAR(255),
	price INT,
	stock INT,
	category_id INT,
	PRIMARY KEY(id),
	FOREIGN KEY(category_id) REFERENCES categories(id));

/* 3. Insert Data */
INSERT INTO users VALUES (null,'John Doe','john@doe.com','john123');
INSERT INTO users VALUES (null,'Jane Doe','jane@doe.com','jenita123');

INSERT INTO categories VALUES (null,'gadget');
INSERT INTO categories VALUES (null,'cloth');
INSERT INTO categories VALUES (null,'men');
INSERT INTO categories VALUES (null,'women');
INSERT INTO categories VALUES (null,'branded');	

INSERT INTO items VALUES (null,'Sumsang b50', 'hape keren dari merek sumsang', 4000000, 100, 1);
INSERT INTO items VALUES (null,'Uniklooh', 'baju keren dari brand ternama', 500000, 50, 2);
INSERT INTO items VALUES (null,'IMHO Watch', 'jam tangan anak yang jujur banget', 2000000, 10, 1);

/* 4. Ambil Data */
/* a. Ambil Data users */
SELECT id, name, email FROM users;

/* b. Ambil Data items */
SELECT * FROM items WHERE price > 1000000;
SELECT * FROM items WHERE name LIKE '%uniklo%';

/* a. Ambil Data items join dengan kategori */
SELECT items.name, items.description, items.price, items.stock, items.category_id, categories.name AS kategori
	FROM items JOIN categories ON items.category_id=categories.id; 

/* 5. Mengubah Data */
UPDATE items SET price = 2500000 WHERE name LIKE '%sumsang%';